#include<iostream> // ����� ��� ����� � ������ ������

#include<cmath> // ����� ��� ������� exp(x), sin(x), log(x)

#include<cstdlib> // ����� ��� ������� rand, ������� ���������� ��������� �����

#include<fstream> // ����� ��� ������ ��������� ��������, ����� ���������� ����� � ���������� �������

#include<ctime>

using namespace std;

double func1(double x) { // ������������ �������

	return (x*x)*sin(x);

}

double func2(double x) { // ��������������� �������

	return (x*x)*sin(x)*sin(5*x);

}

// ������� metod_imit_otzhiga ���� ������� ������� ���������� �������� ������

// � ���������� ���������� � ����

// ��������� �������:

// a, b - ����� � ������ ������� �������, �� ������� ������ �������

// T_max - ��������� �����������

// T_min - ����������� �����������

// f - ��������� �� �������, ����� ������� ����� ���������� ������� func1 � func2

// filename - ������, ����� ������� �������� ��� �����, � ������� ����� ���������� ����������

void metod_imit_otzhiga(double a, double b, double(*f) (double), const char* fn) {

	//srand(time(NULL));

	ofstream fout; //�������� �������� ����� ��� ������

	fout.open(fn); // ������� ���� c ������ fn � ������ ��� � ������� fout

	double x_cur = a + ((double)rand() / RAND_MAX) * (b - a);
	

	// � ���������� x_cur ����� ������� ������� ����� ������� [a, b]

	// x_cur �������� ��������

	double T = 10000;

	// T - ������� �����������, ���������� ����� �� ������ 10000

	double f_cur = f(x_cur); // �������� ������� � ������� �����

	int N = 0; // � ���������� N ����� ������� ���������� ����� ���������

	bool flag = false;//���������� � ������� �������� ������� ����� ��� ��� 

	double x_next, f_next; // x_next - ��������� �����, f_next - �������� � ��������� �����
	x_next = a + ((double)rand() / RAND_MAX) * (b - a); // �������� ���������� ����� ��������� �������
	f_next = f(x_next);
	double delta; // �������� �������� ������� � ������� � � ��������� ������
	delta = f_next - f_cur; // ��������� �������� �������� �������
	double P = exp(-delta / T);
	if (delta <= 0)
	{

		x_cur = x_next; // � ���� ������ ��������� ����� ���������� ������� , ����� �������
		flag = true;
	}
	else flag = false;
	fout << "Tablica: Resultaty poiska extremuma f(x)" << endl; // ������� � ���� �������� �������

	fout << "| N | T |x |f(x) |P | prinita li tochka  " << endl; // ������� ����� �������

	fout << N << "  " << T << "  " << x_cur << "  " << f_cur << "  "  <<  P  << "  " <<  flag << endl;


	// � ��������� ����� ����������� �������� ���������� ��������� �������� ������

	while (T > 0.1) 
	{

		N++; // ����������� �������� �������� N

		x_next = a + ((double)rand() / RAND_MAX) * (b - a); // �������� ���������� ����� ��������� �������

		f_next = f(x_next); // ��������� �������� ������� � ����� x_next

		delta = f_next - f_cur; // ��������� �������� �������� �������
		P = exp(-delta / T);//�����������
		
		

		if (delta <= 0)
		{
		
		x_cur = x_next; // � ���� ������ ��������� ����� ���������� ������� 
		P = 1;
		flag = true;
	     }
		else  if (delta> 0){ // � ��������� ������ ������� ����� �������� � ������������

			double probab = (double)rand() / RAND_MAX; //�������� �������� ����� �� ������� [0,1]

			if (probab <= P) // ���� ��������� ����� �� ����������� exp(-delta / T), ����� ������ ������� �����
			{
				x_cur = x_next;
				flag = false;
			}
		}
		

		T = T * 0.95; // �������� �����������

		if (T > 0.1)

			fout << N << "  " << T << "   " << x_cur << "    " << f(x_cur) << "    " << P <<"       " << flag <<  endl;

		// ���������� � ���� ������� ����������

		else

			fout << "Xmin = " << x_cur << " Fmin = " << f(x_cur) << endl; // ���������� ����� � ����

	}

	fout.close(); // ��������� ����

}

// � ������� main �������� ������� metofd_imit_otzhiga ��� ����

// ��� ������� func1 � func2

int main() {

	metod_imit_otzhiga(9.0, 12.0, func1, "func1.txt");

	metod_imit_otzhiga(9.0, 12.0, func2, "func2.txt");
		return 0;
}